#!/usr/bin/python3

import socket
from socket import AF_INET, SOCK_STREAM, SO_REUSEADDR, SOL_SOCKET, SHUT_RDWR
import ssl
import sys


listen_addr = '127.0.0.1'
listen_port = 8082
server_cert = '../authClient/exemple+3.crt'
server_key = '../authClient/exemple+3-key.pem'
client_certs = '../authClient/exemple+3.crt'

context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
#context.load_cert_chain('server.csr', 'server.key')
# context.load_cert_chain('server.cert', 'server.key')
# context.load_cert_chain(mycert, mykey)

context.load_verify_locations(cafile=client_certs)

try:
    #context.load_cert_chain(mycert, mykey)
    context.load_cert_chain(server_cert, server_key)
    print("All OK!")
except:
    print(sys.exc_info())
    e = sys.exc_info()[0]
    print("input certificates are not correct", e)

bindsocket = socket.socket()
bindsocket.bind((listen_addr, listen_port))
bindsocket.listen(5)

while True:
    print("Waiting for client")
    newsocket, fromaddr = bindsocket.accept()
    print("Client connected: {}:{}".format(fromaddr[0], fromaddr[1]))
    conn = context.wrap_socket(newsocket, server_side=True)
    print("SSL established. Peer: {}".format(conn.getpeercert()))
    buf = b''  # Buffer to hold received client data
    try:
        while True:
            data = conn.recv(4096)
            if data:
                # Client sent us data. Append to buffer
                buf += data
            else:
                # No more data from client. Show buffer and close connection.
                print("Received:", buf)
                break
    finally:
        print("Closing connection")
        conn.shutdown(socket.SHUT_RDWR)
        conn.close()
