#!/bin/bash

#Required
domain=$1
commonname=$domain

#Change to your company details
country=FR
state=NA
locality=Bordeaux
organization=sensitive-vision
organizationalunit=IT
email=contact.sensitivevision@gmail.com

#Optional
password=dummypassword

if [ -z "$domain" ]
then
    echo "Argument not present."
    echo "Usage $0 [common name]"

    exit 99
fi

echo "Generating key request for $domain"
openssl genrsa -des3 -passout pass:$password -out $domain.key 1024 

echo "Removing passphrase from key"
openssl rsa -in $domain.key -passin pass:$password -out $domain.key

echo "Creating CSR"
openssl req -new -key $domain.key -out $domain.csr -passin pass:$password \
    -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

